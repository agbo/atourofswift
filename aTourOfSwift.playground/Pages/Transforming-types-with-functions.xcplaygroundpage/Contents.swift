//: [Previous](@previous)

/*:
 
 # Functions 101
 Now that you've taken your first steps in *Swift*, it's time to introduce one of the most basic, yet most powerful building blocks of *Swift*: functions. 
 
 In the previous lesson, you saw how you can use let to bind a name to a value, but you can also bind a name to a function.
 
 */


func add(x: Int, y: Int)->Int{
    return x + y
}

add(x: 2, y: 4)


/*:
 Functions **require** *type declarations* on all parameters as well as their result. None of them can be omitted.
 
 You might have noticed that in *Swift*, when calling a function, you must include the parameter name. This might seem a little odd for someone coming from another language, but it allows to create very explicit code. You will see an example pretty soon.
 
 
 ## External and internal parameter names
 
 *Swift* has a unique feature not found in any other language know by man: parameters can have 2 names. One is visible only inside the function while the other is visible only outside.
 
 */

func fractionOf(numerator a: Double, denominator b: Double) -> Double{
    return a / b
}

fractionOf(numerator: 42, denominator: 31)

/*:
 
 The ```fractionOf``` function has 2 parameters, each with 2 different names. ```a``` is only visible inside the function, while ```numerator```is only visible outside. This is used to create functions that read like English text, making its purpose self-explanatory.
 
 The syntax of the function ```add```,is a special case, where the internal and external names are the same (x & y). 
 
 ### A parameter has no name
 
 Sometimes, you might want a parameter to have no external name. You can easily achieve this with the *anonymous symbol (_)*. I told you it would be useful someday! 😊 Actually, we'll find more uses for the underscore as we move along.
 */

func add42(_ a: Int)-> Int{ // No external name for a!
    return a + 42
}
add42(9)

/*:
 ## Partial Functions
 
 There's a substantial difference between ```add()```and ```fractionOf()``` and it has nothing to do with parameter names. It's something far more important.
 
 What would happen if you called ```fractionOf``` with ```0```as the denominator? You guessed it! Your app will drop dead instantly. That's not good. Not good at all. 😞
 
 The main differences is that ```fractionOf``` does not work with **all possible values** of its paramters. This is called a [partial function](https://en.wikipedia.org/wiki/Partial_function).
 
 A **partial function** is a function that is not defined for all possible arguments.
 
 Before going ahead and blindly running the function, we must check that the parameters are OK. We will deal with this later on, in the *Error Handling* lesson.
 
 For the time being, just keep it in mind.
 
 */
//: [Next](@next)
