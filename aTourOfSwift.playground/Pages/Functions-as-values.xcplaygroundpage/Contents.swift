//: [Previous](@previous)

/*:
 # Functions as values
 
 
 Like many other modern languages, such as [F#](http://www.fsharp.org), [Kotlin](http://www.kotlinlang.org) or [Haskell](https://www.haskell.org), *Swift* functions are *first-class citizens* of the language. This means that functions are just like any other value and may be:
 1. returned from another function
 1. saved in a collection (such as an array)
 1. be a parameter of another function
 
 
 */

//: [Next](@next)
