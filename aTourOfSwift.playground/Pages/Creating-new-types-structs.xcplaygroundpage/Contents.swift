//: [Previous](@previous)

/*:
 
 # Structs
 
 What happens when you combine a cat with an octopus? You get the *Octocat*!
 
![All hail the mighty octocat!](octocat.png)
 
 Similarly, when you combine *types* with *functions*, you get comething called *Structures*, or *Structs* for short.
 
 
 *Structs* improve on *Tuples* in 2 different ways:
 1. All components have a name. Always. This improves readability and self-documentation.
 1. *Structs* can have functions that operate on the composed types. These are called *methods*.
 
 Let's create a simple *struct* to represent a Complex Number:
 
 ![Complex Number](Complex_number.png)
 
*/



struct Complex{
    let x   : Double
    let y   : Double
}

/*:
 
 A *constructor* or *initializer* is a *method* that creates instances of a *struct*. If you do not provide one, the compiler will create a default one:
*/

let a = Complex(x: 1, y: 4)

/*:
 Not all Complex numbers will have both real and imaginary parts as non-zero. So it might be convenient to provide our own *initializers* :
 1. One only for the real part
 1. Another one just for the imaginary part
 
 As soon as you provide your own initializers, the compiler stops adding the default one. Therefore, you need to provide the initializer that takes as many arguments as components the structure has.
 
 There are many operations on Complex Numbers. These should be modeled as *methods* of the Complex structure. For the sake of brevity, let's create just one: the [magnitude of the complex number](https://en.wikipedia.org/wiki/Complex_number).
 */

struct ComplexNumber{   // Can't have 2 defiitions with the same name
    let x   : Double
    let y   : Double
    
    // Initializers
    init(real: Double, imaginary: Double){
        x = real
        y = imaginary
    }
    
    init(real:Double){
        self.init(real: real, imaginary: 0)
    }
    
    init(imaginary: Double) {
        self.init(real: 0, imaginary: imaginary)
    }
    
    // Operations
    
    func magnitude() -> Double{
        return ((x * x) + (y * y)).squareRoot() // A method of the Double Struct!
    }
}

/*:
 ## Structs & Immutability
 
 By default *structs* are immutable. For a method to modify an instance of a *struct*, it must be specifically marked as **mutating**
 
 */

struct Point{
    var x : Double
    var y : Double
    
    mutating func moveTo(x: Double, y: Double){
        self.x = x
        self.y = y
    }
}

/*:
 ## Structs are never shared
 
 Structs are never shared among variables. Whenever a struct is assigned to a new variable, passed to a function or returned from a function, a fresh new copy is made.
 
 Types that behave this way are called *value types*. Most types in Swift are *value types*.
 */

let origin = Point(x: 0, y: 0)
var other = origin  // other is a copy of origin
dump(origin)
dump(other)
other.moveTo(x: 4, y: 89)
dump(origin)    // origin was not changed by this, only "other"
dump(other)

/*:
 ## Structs in Swift
 
 Structs are usually the default tool to represent things in Swift. Many of the types that come with Swift are implemented as structs.
 
 All the follwoing types are implemented as structs:
 1. Int
 1. Float
 1. String
 1. Double
 1. Array
 1. and many more
 
 Even though *structs* have many advantages and are the way to go if you're using Swift only code, it's a bit like the new USB-C port in recent Macs. In order to use it in the real world, outside of the ivory tower, you need shipload of dongles and adapters:
 
 ![A good solution for tomorrow, but right now it's just not practical](usbc.jpg)
 
 In order to create Apps for iOS, macOS, tvOS and watchOS, you must use many frameworks (both from Apple and 3rd parties). Some of these frameworks are UIKit, Core Data, AppKit, Realm or Firebase.
 
 All of those frameworks expect *classes* instead of *structs*. Therefore, most of the time, you will be using *classes*. We'll learn more about them in the another chapter.
 */

//: [Next](@next)
