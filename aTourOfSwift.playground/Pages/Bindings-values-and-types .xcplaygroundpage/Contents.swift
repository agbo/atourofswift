//: [Previous](@previous)

/*:
 
 
 # Bindings, Values & REPLs!
 A playground is a type of REPL (Read, Eval, Print, Loop) that allows you to explore the language at your leisure.
 Type any Swift expression, and you should see the result in the right hand column. If there's an error, you'll see a description on the bottom of the screen.
 Now, type the following line of code:
 ```let answer = 40 + 2```
 */
let answer = 40 + 2

/*:
 
 ```let``` creates a *binding* between a symbol (```answer```) and a value (```42```). This binding **cannot be changed!** It's similar to a ```const```  in other languages.
 Try typing the following and you should get an error:
 ```let answer = 45```
 
 ## Mutability
 Even though you are *strongly* encouraged to create **inmutable** bindings, there are times when you must be able to change (mutate) a binding. Swift allows for this with the ```var``` keyword.
 
 */

var donnaMobile = "I'm a variable. I can change any time!"
donnaMobile = "I just changed my mind"


/*:
 
 ## Not everything can be changed
 The value of the binding can be changed, but you can't re-define it. Try typing this and you should get an error:
 ```var donnaMobile = "Yo, yo, yo!"```
 
 Now, try changing the value of ```donnaMobile``` to ```56```. You should get an interesting error.
 
 
 # Oh my God, it's full of types!
 
  ![Types, types everywhere!](my-god-its-full-of-stars.jpg)
 
 
 
 Between the REPL and the lightweight ```let``` syntax, it's easy to mistake *Swift* for a dynamically typed language, such as Ruby, Javascript or Python. However, *Swift* is a statically typed language, such as *Typescript*, *Haskell*, *Java* or *C++*.
 
 *Swift's* type system will help you avoid common errors as your software becomes more complex.  However, you shouldn't rely only on the type system.To create quality software, you must also test it.
 
 You can fully declare the type of a binding with colon operator:
 
 */

let name : String = "John Snow"

/*:
 
 This is usually not necessary as the compiler can *infer* the type of a binding by inspecting the value. In this case, it's obvious it will be a string, as "John Snow" is a string.
 
 Whenever the compiler needs extra information from you to determine the type of a symbol, it will let you know with an error.
 
 
 Create a few bindings with different types, such as Double, Float, Int and String. Find out if the compiler can infer the type by itself.
 
 Try the ```dump()``` function to print things, such as ```dump(name)```.
 
 
 ## A type by another name (typealias)
 
 ```typealias``` allows us to give a type another name, an alias.
 
 ```typealias NewName = OldName```
 */

typealias Author = String
typealias Integer = Int

let grrm : Author = "George Raymond Richard Martin"
dump(grrm)

/*:
 
Although ```typealias``` might seem trivial, it will reveal itself as a powerful tool pretty soon.
 
 
 
 # The anonymous symbol: _
 
 The _ (underscore) is a very special symbol in *Swift*: anything you bind to it, is discarded. In Unix terms, it's like sending output to ```/dev/null```
 */


let _ = "Robb Stark"
var _ = "Stannis"
// Both Robb and Stannis are gone.





/*:
 
 Just like ```typealias```, this might seem silly and useless. However, it will turn out to be a very powerful feature and you'll be using it very often. Trust me. 😊
 
 
 [my-god-its-full-of-stars]:my-god-its-full-of-stars.jpg
 */



//: [Next](@next)
