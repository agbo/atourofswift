//: [Previous](@previous)

/*:
 
 # Classes
 
 While *structs* are never shared (which helps prevent many bugs), *classes* on the other hand can be shared among several variables. The other main difference is that *structs* can only be extended via *protocols*.  *Classes* can be extended via:
 
 1. Protocols
 1. Inheritance
 
 While writing *Swift* code, you should use *structs* whenever it's possible, but in many cases you will have to use *classes*. Almost every single framework that target's the Apple set of platforms uses *classes*:
 1. Core Data
 1. UIKit
 1. Realm
 1. Firebase
 1. etc...
 
 
 ## Class Declaration
 
 Very similar to a *struct*:
*/

import AppKit

class AnonymousBook{
    let title       : String
    
    init(title: String) {
        self.title = title
    }
}

/*:
 In Swift, classes may have 0 or 1 base classes.  *AnonymousBook* has no base class. Let's create a subclass of *AnonymousBook*
 */

class Book : AnonymousBook{
    
    let authors : [String]
    
    init(title: String, authors: [String]) {
        // First init your own properties
        self.authors = authors
        
        // Once you've "cleaned after yourself", you may call super
        // the rest of the properties (introduced by your super class
        // may be initialized also)
        super.init(title: title)
        
    }
}

/*:
 ## Convenience initializers
 
 These are initializers for common special cases. For example, we might commonly have *Books* that only have one author. It would be convenient if we could just pass a single String, instead of an Array of Strings.
 
 Unlike regular initializers (called *designated initializers*), convenience initializers **may not call super**.
 
 */

extension Book{
    convenience init(title: String, author: String) {
        self.init(title: title, authors: [author])
    }
}

/*:
 ## Final classes
 
 If you know for sure that a class will *never* be extended, you may mark it as *final*. This allows for some optimizations of your code.
 
 */

final class BookCharacter{
    let name : String
    let book : Book
    
    init(name: String, book: Book){
        self.name = name
        self.book = book
    }
}

/*:
 ## Protocols
 
 Clases may also implement protocols.
 */


extension BookCharacter : CustomStringConvertible{
    var description: String {
        return "<\(type(of:self)): \(name). Appears in \(book)>"
    }
}


/*:
 ## Overriding
 
 Whenever a subclass specializes a method of its superclass, the method implementation must be preceded by the *override* keyword.
 */

class MyView : NSView{
    
    override func viewDidHide() {
        super.viewDidHide()
        
        // Do something special here.
    }
}



//: [Next](@next)
