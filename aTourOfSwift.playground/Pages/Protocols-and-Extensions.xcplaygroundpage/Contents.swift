//: [Previous](@previous)


/*:
 
 # Extensions
 
 Extensions allow you to add behavior to add behavior to pre-existing types. You may add just about anything that doesn't change the memory layout (size) of the type.  
 
 Things you may add:
 1. Methods
 1. [Computed properties](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Properties.html)
 1. [Subscripts](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Subscripts.html)
 
 On the contrary, you may not add [stored properties](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Properties.html) to a type. 
 
 For instance, say the **int** type takes *n bytes* of memory. If you could add a stored property called *description* of type *String*, suddenly *all Ints would now be bigger (n bytes + the size of the String)*! This would overwrite existing memory and your application would drop dead instantly.
 
 ## Syntax for an extension
 */

// Adding behavior to a pre-existing struct 
extension Int{
    func invert() -> Int{
        return -self
    }
}

// From now on, all Ints have the invert() method
4.invert()

// Adding behavior to a struct you created yourself
struct Complex{
    let x: Double
    let y: Double
}

extension Complex{
    var magnitude : Double{
        return ((x * x) + (y * y)).squareRoot()
    }
}


/*:
 Extensions are frequently used in the way above to organize code and keep the core or your structs as simple as possible. Check the [Swift Language Source Code](https://github.com/apple/swift).
 */



/*:
 # Protocols
 
 [Protocols in *Swift*](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Protocols.html) are what other languages call *interfaces*.
 
 They are alike a contract that establishes what functionalities a type must have in order to perform a given task. A type is said to *adopt* a protocol when it enforces this *contract*.
 
 Protocols are a cornerstone of Swift. Swift is even frequently described as a *protocol oriented language*.
 
 Almost all functionalities of Swift types, that seem like *compiler magic* are actually implemented by *adopting* a protocol.
 
 Extensions and protocols are frequently used together, as it's a common pattern to use an extension to adopt a protocol.
 
 ## Common Protocols in Swift
 
 You should familiarize yourself with some of the most common protocols in Swift:
 1. **CustomStringConvertible**: Provides a string representation of your type
 1. **Equatable**: Provides an implementation of the == operator. An *Equatable* type can tell when 2 instances are equal
 1. **Hashable**: Provides a hash value. All Equatable types should implement hashable too.
 1. **Comparable**: Allows 2 instances of a type to tell which one is bigger.
 
 Let's make *Complex* enforce the first 3, each on a different extension:
 */

extension Complex: CustomStringConvertible{
    var description: String {
        return "(\(x), \(y)i)"
    }
}

let a = Complex(x: 3, y: -9)
let b = Complex(x: -2, y: 3)

extension Complex : Equatable{
    static func ==(lhs: Complex, rhs: Complex)-> Bool{
        return (lhs.x, lhs.y) == (rhs.x, rhs.y)
    }
}

(a == a)
(a == b)

extension Complex : Hashable{
    var hashValue: Int {
        return x.hashValue & y.hashValue
    }
}


/*:
 #### What about *Comparable*?
 
 This is a bit tricky with complex numbers, as the lie on a plane, and can't be sorted as Natural numbers.
 If we still need to have a sorted array of complex numbers, we could sort them by their magnitude.
 */

extension Complex : Comparable{
    static func <(lhs: Complex, rhs: Complex) -> Bool{
        return lhs.magnitude < rhs.magnitude
    }
}

/*:
 The *Comparable* protocol requires many other functions, not just *<*. However, the compiler seems to by happy with just one function. Try to find out why.
 */


/*:
 ## Protocols with default implementations
 
 Last but not least, protocols can have a default implementation.
 */

// Our very own protocol
protocol Answerable{
    var answer : Int{get}
}

// Default implementation
extension Answerable{
    var answer : Int{
        return 42
    }
}


extension String : Answerable {}

"What is the meaning of Life, Universe and everything?".answer


//: [Next](@next)
