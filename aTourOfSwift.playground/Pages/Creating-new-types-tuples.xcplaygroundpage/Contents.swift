//: [Previous](@previous)

/*:
//: # Creating new types
 
 By glueing together simple types (such as Ints, Doubles, Strings, etc...), we can create new, more complex types. This is a *Fundamental Concept in Sofftware Engineering: Composition*. 
 
 Actually, that's just a fancy name for the Lego Bricks strategy you already know since you where a kid: start with simple elements (bricks) and combine them to create more complex elements. Combine those slightly more complex elements to create even more complex ones. Keep doing this until you reach the level of complexity you need.
 
 ![It all started with a humble block!](star-wars-lego.jpg)
 

 
 In this introduction to *Swift*, we will learn about 4 different ways of composing new types:
 1. Tuples
 1. Structs
 1. Classes
 1. Enums
 
 
 Let's start with the simplest one, the humble *Tuple*.
 
 ## Tuples

 ![A tuple of different people](line-tuple.jpg)
 
 The *Tuple* is by far the easiest way to combine types to create a new one: just put them in a line!
 
 Notice that we have different *types* in our line: male and female. That's OK, you can make a *tuple* out of different types in a row.
 
 */

var httpResponse = ("OK", 200)
httpResponse = ("Internal Server Error", 500)
httpResponse = ("Enhance Your Calm", 420)

/*:
 The tuple has 2 different components: a String and an Int.
 
 You may access the components by position, with the ```dot``` operator:
 */

httpResponse.0
httpResponse.1
dump(httpResponse)

/*:
 ### Tuples have types too!
 
 Each *tuple* has its type. For instance, the type of ```httpResponse``` is ```(String, Int)```. Try to assign a different tuple (a tuple with 3 elements, or a tuple of 2 Ints, and you'll get an error. 
 
 Go ahead, give it a try!
 
 */
//httpResponse = (88, 23, "Yo")   // This won't fly! It's a different type (Int, Int, String)

/*:
 The elements of a Tuple can have a name too, and be accessed by that name. Let's make a Complex Number out of 2 Doubles:
 */
typealias ComplexNumber = (real: Double, imaginary: Double) // See how typealias can be useful to make things simpler?

let n : ComplexNumber = (0.0, 32)
n.real
n.imaginary
dump(n)

/*:
 ### Use cases for Tuples
 
 *Tuples* are great when you want to create a simple and sort-lived association between 2 types. It can be used to represent Complex Numbers, as long as you don't need to represent operations between Complex numbers (additon, multiplication, etc...). If you need these extra features, then the humble *Tuple* won't cut the mustard. You'll have to upgrade it to a Struct (more on that later).
 
 *Tuples* however, are great as a return type of functions. This way you can simmulate functions that return more than one value.
 
 Let's try a simple example. A function that divides a number and returns the integer and the remainder:
 */

// Integer division
func intDiv(_ a: Int, _ b: Int) -> (Int, Int){
    return ((a / b), (a % b))
}

intDiv(3,2)

/*:
 
 Let's try a more elaborate example. We will create a very simple httpServer that takes requests and returns responses.
 */

// A simple http server. Receives a request and returns a response along with a status
// BTW, are you starting to appreciate how typealias makes our code look cleaner?
typealias HTTPStatus    = (code: Int, text: String)
typealias HTTPRequest   = String                                // We don't need anything fancier at this time
typealias HTTPResponse  = (body: String, status: HTTPStatus)    // A Tuple with another tuple inside. Well yes, why not?


func httpServer(request: HTTPRequest)->HTTPResponse{
    // Stub. This is where all the hardwork should happen
    
    return ("It worked",  ( 200,  "OK"))
}

let response = httpServer(request: "index.html")
dump(response)


/*:
 As seen in dump of the ```response``` constant, we got the whole ```HTTPResponse``` tuple as the return value. This shouldn't be surprising, but sometimes you might only want a part of the tuple (say, the status code and the status message).
 
 This is where one of the coolest features in *Swift*, **pattern matching**, comes handy along with the *anonymous symbol* (underscore).
 
 */

// The tuple will be disassembled and each part assigned to a constant:
let (result, remainder) = intDiv(5,2)
print(result)
print(remainder)

// I only care for the remainder
let (_, rm) = intDiv(42, 5)
print(rm)

// Let's try a more complex example. I only care about the status of the http response
let (_, status) = httpServer(request: "index.php")
dump(status)

// Now, lets extract the status code using pattern matching
let (code, _) = status
dump(code)

/*:
 
 ![0-tupe, 1-tuple, 2-tuple](tuples.jpg)
 ### One last thing! One tuple, 2 tuple, 3 tuple
 
 Tuples have different names depending on their size (number of elements). For instance, tuples with two elements, ```(2, "two")```, are called 2-tuples. Similarly, tuples with 3 elements are called 3-tuples. You get the idea.
 
 There are, however, a few special cases: 1-tuple and 0-tuple.
 
 #### 1-Tuple
 
 In many languages, a 1-tuple is exactly what you would expect: a tuple with one element. Not so in *Swift*. 1-Tuples don't exist in *Swift*. If you try to insert a value inside a 1-Tuple, you get that value back:
 */
(999) // this is actually 999

/*:
 #### 0-Tuple
 
 This is the empty tuple. In *Swift*, the empty tuple is something equivalent to Void in other languages. It means the lack of a value.
 For instance, a function that has no return is actually a function that returns the empty tuple: ()
 */

// These 2 functions are exactly equivalent
func f(a:Int){
    print(a)
}

func h(a: Int) -> (){
    print(a)
}

let nothing = f(a:34)
dump(nothing) // 0-tuple ()

//: [Next](@next)
