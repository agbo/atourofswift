
/*:
 # A Tour of Swift 4.0
 *Copyright Keepcoding.io* All rights reserved.
 
 
 1. [Bindings, values and types](Bindings-values-and-types)
 1. [Transforming types with functions](Transforming-types-with-functions)
 1. [Creating new types: tuples](Creating-new-types-tuples)
 1. [Creating new types: Structs](Creating-new-types-structs)
 1. [Protocols & Extensions](Protocols-and-Extensions)
 1. [Creating new types: classes](Creating-new-types-classes)
 
 
 
 */


//: [Next](@next)
